﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPanelScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject[] cubeBeatList = GameObject.FindGameObjectsWithTag("RealBeat");
        if (cubeBeatList != null || cubeBeatList.Length > 0)
        {
            cubeBeatList[0].GetComponent<SoundSpectrumBeat>().onSongEnd = this.reEnablePanel;
        }
        else
        {
            Debug.LogError("couldn't find beat cube, no sound was loaded ???");
            // do nothing
        }
        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        // nothing here !
    }

    public void reEnablePanel()
    {
        this.gameObject.SetActive(true);
        // check game state
        GameObject[] veggies = GameObject.FindGameObjectsWithTag("vegetable");
        Transform childMessage = this.gameObject.transform.Find("EndPanelMessage");
        if (childMessage == null)
        {
            Debug.LogError("no panel was found, game *might* be broken");
        }
        else
        {
            if (veggies.Length > 0 )
            {
                childMessage.gameObject.GetComponent<TextMesh>().text = "Victory!";
            }
            else
            {
                childMessage.gameObject.GetComponent<TextMesh>().text = "Game\nOver";
            }
        }
    }
}
