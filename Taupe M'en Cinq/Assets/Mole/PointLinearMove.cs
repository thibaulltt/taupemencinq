﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointLinearMove : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> pathPoints = new List<GameObject>();
    private List<GameObject>.Enumerator destination;

    //Variable to keep track if the destination was reached or not.
    
    private bool destinationReached;

    public System.Action onDestinationReached;

    [Tooltip("Speed value in meter per second of the object following the path.")]
    public float speed = 0.5f; 
    // Start is called before the first frame update
    void Start()
    {
        destination = pathPoints.GetEnumerator();
        if(pathPoints.Count == 0) {
            destinationReached = true;
            onDestinationReached();
        }
        else {
            destination.MoveNext(); // first element
            transform.position = destination.Current.transform.position;
            destinationReached = destination.MoveNext() == false; // second element
        }
    }

    public void SetPathPoints(List<GameObject> newPathPoints) {
        pathPoints = newPathPoints;
        Start();
    }

    public GameObject GetPathPoint(int index) {
        return pathPoints[index];
    }

    public int GetPathSize()
    {
        return pathPoints.Capacity;
    }

    public void Clear()
    {
        destinationReached = true;
        pathPoints = new List<GameObject>();
        destination = pathPoints.GetEnumerator();
    }

    // Update is called once per frame
    void Update()
    {
        if(destinationReached) {
            return;
        }
        else {
            // move length to do at this update
            float moveLength = Time.deltaTime * speed;
            Vector3 goalPos = destination.Current.transform.position;
            Vector3 currentPos = transform.position;
            Vector3 dir = (goalPos - currentPos).normalized;
            
            float distanceToGoal = Vector3.Distance(goalPos, currentPos);
            while(moveLength > 0 && !destinationReached) {
                // move length to do at this frame but no farther that next destination point.
                float moveLengthDest;
                if(moveLength < distanceToGoal) {
                    moveLengthDest = moveLength;
                }
                else {
                    moveLengthDest = distanceToGoal;

                    // If there is another point to the path
                    if(destination.MoveNext()) {
                        goalPos = destination.Current.transform.position;
                        dir = (goalPos - currentPos).normalized;
                        distanceToGoal = Vector3.Distance(goalPos, currentPos);
                    }
                    else {
                        destinationReached = true;
                    }
                }

                Vector3 moveVector = dir * moveLengthDest;

                transform.position += moveVector;

                if(!destinationReached)
                {
                    this.transform.forward = (goalPos - this.transform.position).normalized;
                }

                moveLength -= moveLengthDest;
            }
            if(destinationReached) {
                onDestinationReached();
            }
        }
    }
}
