﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoleBehaviour : MonoBehaviour
{
    public AnimationClip ungroundAnimation;

    private GameObject spawnPoint;
    private GameObject discoverPoint;
    private GameObject playerPoint;
    private GameObject stealPoint;

    private Animator animator;
    private float speed = 0f;
    private float timeToPlayerPoint;

    [SerializeField, FMODUnity.EventRef]
    private string dirtEventName;
    private FMOD.Studio.EventInstance dirtEvent;

    [SerializeField, FMODUnity.EventRef]
    private string laughEventName;

    [SerializeField, FMODUnity.EventRef]
    private string crocEventName;

    [SerializeField, FMODUnity.EventRef]
    private string popEventName;

    enum State
    {
        Hidden,
        Other
    }

    private State state = State.Hidden;

    public GameObject dirtPilePrefab;
    private float timeSinceLastPile = 0f;
    [Tooltip("The time between each dirt pile to spawn")]
    public float timeBetweenPiles = 0.5f;

    [SerializeField]
    private GameObject star1;
    [SerializeField]
    private GameObject star2;
    [SerializeField]
    private GameObject star3;

    private GameObject chosenVeggie = null;

    // Start is called before the first frame update
    void Awake()
    {
        animator = transform.Find("MoleModel").gameObject.GetComponent<Animator>();

        timeToPlayerPoint = GameObject.FindGameObjectsWithTag("RealBeat")[0].GetComponent<SoundSpectrumBeat>().delay;
        Debug.Assert(timeToPlayerPoint > 0f);

        transform.Find("MoleModel").gameObject.SetActive(false);
    }

    void Start()
    {
        dirtEvent = FMODUnity.RuntimeManager.CreateInstance(dirtEventName);
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(dirtEvent, transform, GetComponent<Rigidbody>());

        dirtEvent.start();
    }

    // Update is called once per frame
    void Update()
    {
        if(state == State.Hidden)
        {
            timeSinceLastPile += Time.deltaTime;
            if(timeSinceLastPile > timeBetweenPiles)
            {
                timeSinceLastPile -= timeBetweenPiles;
                PopDirt();
            }

            float proximity = Vector3.Distance(transform.position, spawnPoint.transform.position) / Vector3.Distance(spawnPoint.transform.position, discoverPoint.transform.position);
            dirtEvent.setParameterByName("Proximity", proximity);
        }
    }

    GameObject PopDirt()
    {
        return Instantiate(dirtPilePrefab, transform.position, transform.rotation);
    }

    GameObject PopBigDirt()
    {
        GameObject dirt = PopDirt();
        dirt.transform.localScale *= 2;
        return dirt;
    }

    public void SetAllPathPoints(List<GameObject> pathPoints) {
        Debug.Assert(pathPoints.Count == 4);
        
        spawnPoint = pathPoints[0];
        discoverPoint = pathPoints[1];
        playerPoint = pathPoints[2];
        stealPoint = pathPoints[3];
        
        float distance = Vector3.Distance(spawnPoint.transform.position, discoverPoint.transform.position) + Vector3.Distance(discoverPoint.transform.position, playerPoint.transform.position);

        speed = distance / (timeToPlayerPoint - ungroundAnimation.length); // Add unground animation time
        
        List<GameObject> fromSpawnToDiscover = new List<GameObject>();
        fromSpawnToDiscover.Add(spawnPoint);
        fromSpawnToDiscover.Add(discoverPoint);

        PointLinearMove path = GetComponent<PointLinearMove>();
        path.SetPathPoints(fromSpawnToDiscover);
        path.onDestinationReached = OnDiscoveryPointReached;
        path.speed = speed;
    }

    private void OnDiscoveryPointReached() {
        // and make mole appear
        transform.Find("MoleModel").gameObject.SetActive(true);
        dirtEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        dirtEvent.release();

        StartCoroutine(OnDiscoveryPointReachedAndUnground());
    }

    IEnumerator OnDiscoveryPointReachedAndUnground()
    {
        yield return new WaitForSecondsRealtime(ungroundAnimation.length);

        state = State.Other;

        List<GameObject> fromDiscoverToSteal = new List<GameObject>();
        fromDiscoverToSteal.Add(discoverPoint);
        fromDiscoverToSteal.Add(playerPoint);
        fromDiscoverToSteal.Add(stealPoint);

        PointLinearMove path = GetComponent<PointLinearMove>();
        path.SetPathPoints(fromDiscoverToSteal);
        path.onDestinationReached = OnStealPointReached;
        path.speed = speed;
    }

    private void OnStealPointReached() {
        GameObject[] scoreElements = GameObject.FindGameObjectsWithTag("Score");
        foreach (GameObject scoreEl in scoreElements)
        {
            Score score = scoreEl.GetComponent<Score>();
            score.AddMissed();
        }
        if(CheckVeggiesOrDie())
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(laughEventName, gameObject);
        }
    }

    private void OnVeggieReached() {
        // TODO
        // - Take veggie
        // - Turn in front of player
        // - Check victory
        // - Make dirt appear and mole go down,
        // - Make dirt disappear
        // - Mole die
        animator.SetTrigger("VeggiePickup");
    }

    public void OnDeathAnimationEnd()
    {
        Destroy(gameObject);
    }

    public float GetDistanceFromIdealPoint()
    {
        return Vector3.Distance(transform.position, playerPoint.transform.position);
    }

    public void PopStars(int number)
    {
        GameObject[] stars = { star1, star2, star3 };
        for(int i = 0; i < 3; ++i)
        {
            if(number > i)
            {
                stars[i].SetActive(true);
            }
            else
            {
                break;
            }
        }
    }

    public void OnVeggiePickupEnd()
    {
        // Attach veggie to mole mouth
        Transform mouth = transform.Find("MoleModel/Mouth");
        chosenVeggie.transform.position = mouth.position;
        chosenVeggie.transform.Rotate(new Vector3(0, 0, 1), 90f);
        chosenVeggie.transform.SetParent(mouth, true);

        FMODUnity.RuntimeManager.PlayOneShot(crocEventName, transform.position);
    }

    public void Die() {
        FMODUnity.RuntimeManager.PlayOneShot(popEventName, transform.position);
        animator.SetTrigger("Death");
        PopBigDirt();
        GetComponent<PointLinearMove>().Clear();
        GetComponent<BoxCollider>().enabled = false;
        // The animation will call OnDeathAnimationEnd at the end
    }

    private bool CheckVeggiesOrDie()
    {
        GameObject[] veggies = GameObject.FindGameObjectsWithTag("vegetable");
        if (veggies.Length > 0)
        {
            // TODO steal action here ...
            PointLinearMove stealPath = GetComponent<PointLinearMove>();
            GameObject lastpathpoint = null;
            for (int i = 0; i < veggies.Length; ++i)
            {
                if (veggies[i].GetComponent<VeggieBehaviour>().TryToTargetVeggie())
                {
                    lastpathpoint = veggies[i];
                    break;
                }
            }
            if (lastpathpoint != null) {
                List<GameObject> newpath = new List<GameObject>();
                newpath.Add(this.stealPoint);

                // TODO - Stop 30cm before the veggie
                newpath.Add(lastpathpoint);

                stealPath.SetPathPoints(newpath);
                chosenVeggie = lastpathpoint;
                stealPath.onDestinationReached = OnVeggieReached;
                return true;
            }
            else
            {
                this.Die();
                return false;
            }
        }
        else
        {
            this.Die();
            return false;
        }
    }
}
