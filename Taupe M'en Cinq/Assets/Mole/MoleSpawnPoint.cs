﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoleSpawnPoint : MonoBehaviour
{
    [Tooltip("Mole prefab.")]
    public GameObject mole;
    
    [Tooltip("Path points for trajectory (this should include the spawner itself).")]
    public List<GameObject> pathPoints;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SpawnAMole() {
        GameObject moleInstance = Instantiate(mole);
        moleInstance.GetComponent<MoleBehaviour>().SetAllPathPoints(pathPoints);
    }

    // Update is called once per frame
    void Update()
    {

    }


}
