﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoleSpawnerBehaviour : MonoBehaviour
{

    [Tooltip("The beat tracker object used to spawn mobs in rhythm.")]
    public GameObject beatTracker;
    private Queue<MoleSpawnPoint> spawners = new Queue<MoleSpawnPoint>();

    // Start is called before the first frame update
    void Start()
    {
        // Get all spawn points
        foreach(GameObject spawner in  GameObject.FindGameObjectsWithTag("MoleSpawn")) {
            spawners.Enqueue(spawner.GetComponent<MoleSpawnPoint>());
        }
        beatTracker.GetComponent<SoundSpectrumBeat>().onBeatHit = SpawnAMole;
    }

    void SpawnAMole() {
        // Spawn mole starting by the spawner that last spawn a mole
        MoleSpawnPoint spawner = spawners.Dequeue();
        spawner.SpawnAMole();
        spawners.Enqueue(spawner);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
