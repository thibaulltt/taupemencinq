﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VeggieBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    [Tooltip("Checks if the veggie is taken by a mole or not")]
    bool isTaken = false;
    void Start()
    {
        // nothing here ...
    }

    // Update is called once per frame
    void Update()
    {
        // nothing here...
    }

    public bool TryToTargetVeggie()
    {
        if (this.isTaken)
        {
            return false;
        }
        else
        {
            this.isTaken = true;
            return true;
        }
    }
}
