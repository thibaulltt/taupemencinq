﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerBehaviour : MonoBehaviour
{
    Vector3 lastPosition;
    double speed; // equals force
    Vector3 dir;
    [Tooltip("The threshold at which a hit has been registered.")]
    public double fastThreshold;
    bool isHitting;

    // Start is called before the first frame update
    void Start()
    {
        this.tag = "Hammer";
        this.isHitting = false;
        this.speed = .0;
        this.lastPosition = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        // update current speed
        this.dir = (this.transform.position - this.lastPosition);
        this.speed = (double)dir.magnitude;
        // get position, set it as last.
        this.lastPosition = this.transform.position;
    }

    private bool IsDirDown()
    {
        float x = Mathf.Abs(dir.x);
        float y = Mathf.Abs(dir.y);
        float z = Mathf.Abs(dir.z);
        return (dir.y < 0 && y > x && y > z);
    }

    private void OnTriggerEnter(Collider other)
    {
        this.isHitting = true;
        if (/*IsDirDown() && */other.tag == "MoleInstance")
        {
            // Do crush action here on mole ...
            other.gameObject.GetComponent<MoleBehaviour>().Die();
            GameObject[] scoreElements = GameObject.FindGameObjectsWithTag("Score");
            MoleBehaviour moleBehaviour = other.GetComponent<MoleBehaviour>();
            foreach (GameObject scoreEl in scoreElements)
            {
                Score score = scoreEl.GetComponent<Score>();
                moleBehaviour.PopStars(score.AddScoreBasedOnDistance(moleBehaviour.GetDistanceFromIdealPoint()));
                
            }
        }
        else
        {
            // Do nothing here.
        }
    }

    private void OnTriggerExit(Collider other)
    {
        this.isHitting = false;
    }
}
