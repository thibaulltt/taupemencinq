using System;
using System.Collections;
using UnityEngine;
using System.Runtime.InteropServices;
using System.IO;
using System.Collections.Generic;
using System.Globalization;

class SoundSpectrumBeat : MonoBehaviour
{
    public Material normalMaterial;
    public Material activeMaterial;
    private bool isActive = false;

    [Tooltip("Path to the file containing the beat reference.")]
    public string beatReferencePath = "/Audio/Beats/notes.txt";

    public float delayForEnd = 2f;

    [Tooltip("The volume percentage (between 0 and 1).")]
    public float volume = 1.0f;

    [Tooltip("The delay to start the music (in seconds).")]
    public float delay = 0.0f;

    private bool started = false;

    public Action onBeatHit = null;

    public Action onSongEnd = null;

    static private int availableId = 0;
    private int id = availableId++;

    private float noteBeginDelay = 0.3f;

    void Start()
    {   
        GetComponent<Renderer>().material = normalMaterial;

        System.IO.StreamReader reader = File.OpenText(Application.dataPath+beatReferencePath);
        string line;
        string[] items = {};
        List<float> notesTime = new List<float>();

        while ((line = reader.ReadLine()) != null)
        {
            items = line.Split('\t');
            float noteTime = float.Parse(items[0], CultureInfo.InvariantCulture.NumberFormat);
            notesTime.Add(noteTime);
        }

        float soundLength = float.Parse(items[2]);

        foreach(float noteTime in notesTime)
        {
            StartCoroutine(DoNote(noteBeginDelay + delay + noteTime));
        }

        StartCoroutine(StartSoundTimerToDetermineEnd(soundLength));
    }

    IEnumerator DoNote(float deltaTime) {
        yield return new WaitForSecondsRealtime(deltaTime);

        if(onBeatHit != null)
        {
            onBeatHit();
        }
        GetComponent<Renderer>().material = activeMaterial;

        StartCoroutine(DisableMaterial());
    }

    IEnumerator DisableMaterial()
    {
        yield return new WaitForSeconds(0.5f);
        GetComponent<Renderer>().material = normalMaterial;
    }

    IEnumerator StartSoundTimerToDetermineEnd(float soundLength)
    {
        yield return new WaitForSecondsRealtime(soundLength+delay+delayForEnd);

        if (onSongEnd != null) { onSongEnd(); }
    }

    const float WIDTH = 10.0f;
    const float HEIGHT = 0.1f;

    float oldLevel = -80f;
    enum Curve
    {
        INCREASING,
        DECREASING
    }
    Curve curve = Curve.DECREASING;

    void Update()
    {
                
    }
}