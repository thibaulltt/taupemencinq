﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeTracker : MonoBehaviour
{
    private bool started = false;
    // Start is called before the first frame update

    [FMODUnity.EventRef]
    public string musicEventName;
    private FMOD.Studio.EventInstance musicEvent;

    void Start()
    {
        musicEvent = FMODUnity.RuntimeManager.CreateInstance(musicEventName);
        musicEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        // StartTracking();
    }

    // Update is called once per frame
    void Update()
    {
        if(!started) {
            return;
        }
        // TODO
    }

    public void StartTracking() {
        started = true;
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(musicEvent, transform, GetComponent<Rigidbody>());
        musicEvent.start();
    }
}
