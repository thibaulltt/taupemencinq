using System;
using UnityEngine;
using System.Runtime.InteropServices;
using System.Collections;

class DelayedMusic : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string musicEventName = null;

    [Tooltip("The delay to start the music (in seconds).")]
    public float delay = 0.0f;
    private FMOD.Studio.EventInstance musicInstance;

    void Start()
    {   
        musicInstance = FMODUnity.RuntimeManager.CreateInstance(musicEventName);
        musicInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(musicInstance, transform, GetComponent<Rigidbody>());

        StartCoroutine(StartDelayed());
    }

    IEnumerator StartDelayed() {
        yield return new WaitForSecondsRealtime(delay);

        musicInstance.start();
    }

    void Update()
    {

    }

    public void SetBadness(float badness)
    {
        musicInstance.setParameterByName("Badness", badness);
    }

    private void OnDestroy()
    {
        musicInstance.release();
    }
}
