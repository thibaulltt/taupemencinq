﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField]
    private Sprite unfilledStar;
    [SerializeField]
    private Sprite filledStar;
    [SerializeField]
    private GameObject star1;
    [SerializeField]
    private GameObject star2;
    [SerializeField]
    private GameObject star3;
    [SerializeField]
    private GameObject scoreText;

    [SerializeField, Tooltip("Minimum percentage of the maximum score to have the first star.")]
    private float star1ScoreRate = 0.20f;
    [SerializeField, Tooltip("Minimum percentage of the maximum score to have the third star.")]
    private float star2ScoreRate = 0.45f;
    [SerializeField, Tooltip("Minimum percentage of the maximum score to have the third star.")]
    private float star3ScoreRate = 0.70f;
    [SerializeField, Tooltip("Maximum distance allowed to score point on hitting a mole.")]
    private float maxDistance = 1f;
    private int scoreStep = 10;
    private int score = 0;
    // maximum score that could be reach by the player if he played perfectly at this time
    private int maximumScore;

    private const int higherSingleScore = 100;

    [SerializeField, Tooltip("Potential game objets holding DelayedMusic component.")]
    private List<GameObject> potentialEars = new List<GameObject>();

    private DelayedMusic music;

    // Start is called before the first frame update
    void Start()
    {
        if(!(star1ScoreRate <= star2ScoreRate && star2ScoreRate <= star3ScoreRate))
        {
            Debug.LogError("Stars rate should be increasing in following order : star1ScoreRate <= star2ScoreRate <= star3ScoreRate");
        }

        foreach(GameObject potentialEar in potentialEars)
        {
            if(potentialEar.activeInHierarchy)
            {
                music = potentialEar.GetComponent<DelayedMusic>();
                break;
            }
        }
    }

    /**
     * @return the number of star that this hit is worth.
     **/
    public int AddScoreBasedOnDistance(float distance)
    {
        float scoreF = (Mathf.Max(maxDistance - distance, 0f) / maxDistance) * higherSingleScore;
        int scoreToAdd = Mathf.CeilToInt(scoreF / scoreStep) * scoreStep;
        SetScore(score + scoreToAdd);
        SetMaximumScore(maximumScore + higherSingleScore);
        return ComputeStarNumber(scoreToAdd, higherSingleScore);
    }

    public void AddMissed()
    {
        SetMaximumScore(maximumScore + higherSingleScore);
    }

    private void SetScore(int newScore)
    {
        score = newScore;
        scoreText.GetComponent<TextMesh>().text = score.ToString();
    }

    private void SetMaximumScore(int newMaximumScore)
    {
        maximumScore = newMaximumScore;
        int starNb = ComputeStarNumber(score, maximumScore);
        SetStarNumber(starNb);
    }

    private int ComputeStarNumber(int score, int maxScore)
    {
        float[] starRates = { star1ScoreRate, star2ScoreRate, star3ScoreRate };
        int starNb = 0;
        float scoreRate = (float)score / (float)maxScore;
        foreach (float starRate in starRates)
        {
            if(scoreRate >= starRate)
            {
                starNb++;
            }
            else
            {
                break;
            }
        }
        return starNb;
    }

    private void SetStarNumber(int number)
    {
        Debug.Assert(0 <= number && number <= 3);

        GameObject[] stars = { star1, star2, star3 };

        for(int i = 0; i < 3; ++i)
        {
            if(number > i)
            {
                stars[i].GetComponent<SpriteRenderer>().sprite = filledStar;
            }
            else
            {
                stars[i].GetComponent<SpriteRenderer>().sprite = unfilledStar;
            }
        }

        if(music)
        {
            switch(number)
            {
                case 0:
                    music.SetBadness(0.5f);
                    break;
                case 1:
                    music.SetBadness(0.3f);
                    break;
                case 2:
                    music.SetBadness(0.1f);
                    break;
                default:
                    music.SetBadness(0f);
                    break;
            }
        }
        else
        {
            Debug.Log("Cannot found the delayed music in the scene");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(music == null)
        {
            foreach (GameObject potentialEar in potentialEars)
            {
                if (potentialEar.activeInHierarchy)
                {
                    music = potentialEar.GetComponent<DelayedMusic>();
                    break;
                }
            }
        }
    }
}
