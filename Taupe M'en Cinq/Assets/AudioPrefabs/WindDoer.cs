﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;

public class WindDoer : MonoBehaviour
{
    [SerializeField, FMODUnity.EventRef]
    private string windEventName;

    [SerializeField, Tooltip("Minimum time between two wind event.")]
    private float minTime = 1.0f;
    private float windSoundLength;

    void Start()
    {
        StartCoroutine(RandomlyStartWinding());

        FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(windEventName);
        FMOD.Studio.EventDescription description;
        eventInstance.getDescription(out description);
        int windSoundLengthMs;
        description.getLength(out windSoundLengthMs);
        windSoundLength = windSoundLengthMs / 1000f;
        eventInstance.release();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator RandomlyStartWinding()
    {
        for(; ; )
        {
            if(Random.value < 0.5f)
            {
                GameObject[] trees = GameObject.FindGameObjectsWithTag("Tree");
                GameObject windingTree = trees[Mathf.RoundToInt(Random.Range(0, trees.Length))];

                Animator treeAnimator = windingTree.GetComponent<Animator>();
                if(treeAnimator != null)
                {
                    treeAnimator.SetBool("Wind", true);
                }
                StartCoroutine(SetWindingTreeToNormal(windingTree));

                FMODUnity.RuntimeManager.PlayOneShotAttached(windEventName, windingTree);
            }
            yield return new WaitForSeconds(minTime);
        }
    }

    IEnumerator SetWindingTreeToNormal(GameObject tree)
    {
        yield return new WaitForSeconds(windSoundLength);

        Animator treeAnimator = tree.GetComponent<Animator>();
        if (treeAnimator != null)
        {
            treeAnimator.SetBool("Wind", false);
        }
    }
}
