﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DancingTrees : MonoBehaviour
{
    GameObject[] trees;
    // Start is called before the first frame update
    void Start()
    {
        trees = GameObject.FindGameObjectsWithTag("Tree");
        GetComponent<SoundSpectrumBeat>().onBeatHit = DoTreeAnim;
    }

    void DoTreeAnim()
    {
        foreach(GameObject tree in trees)
        {
            tree.GetComponent<Animator>().SetTrigger("Beat");
        }
    }
}
